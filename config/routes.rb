Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'csvupload#index'

  post 'barcode', to: "csvupload#barcode"
  get 'pdfbarcode', to: "barcode#index"
end
