class BarcodeController < ApplicationController
    def index
        tabelajson = params[:tabela]
        barcodesData = JSON.parse tabelajson

        @codes3 = []
        @codes2 = []
        @codes1 = []

        barcodesData.each do |product|
            nun_cod1 = product[4].to_s
            nun_cod2 = product[5].to_s
            nun_cod3 = product[6].to_s

            if nun_cod1.present?
                nun_cod1c = nun_cod1[0...-1]
                barcode1 = Barby::EAN13.new(nun_cod1c)
                barcode_voorbeeld1 = Barby::HtmlOutputter.new(barcode1)
                barcode_htmlstring1 = barcode_voorbeeld1.to_html

                cod1 = '<div class="cod-1">
                        <div class="holder-ean">
                            <div class="side-bar">
                                <div class="head-info">
                                    <div class="mart-logo">'+ActionController::Base.helpers.image_tag("mart-logo.png")+'</div>
                                    <div class="po"><p>'+product[3].to_s+'</p></div>
                                </div>
                                <p class="cod-name">'+product[1].to_s+' - '+product[2].to_s+'</p>
                                '+barcode_htmlstring1.html_safe+'
                                <p class="cod-bottom">'+product[4].to_s+'</p>
                            </div>
                            <div class="side-text">
                                <p>MATERIAL: '+product[0].to_s+'</p>
                                <p>VALIDADE INDETERMINADA</p>
                                <p>GARANTIA 90 DIAS</p>
                                <p>CONTÉM: '+product[7].to_s+'</p>
                                <p>IMPORTADO POR</p> 
                                <p>MOAS IND.COM.IMP.EXP.LTDA.</p>
                                <p>RUA TENENTE SPICCIATI, 174</p> 
                                <p>BARRA FUNDA-SP CEP:01140-130<p>
                                <p>CNPJ.: 05.388.725/0001-12</p>
                                <p>SAC.MART@GRUPOMOAS.COM.BR</p>
                                <p>PRODUZIDO NA CHINA</p>
                            </div>
                        </div>
                    </div>'
                @codes1 << cod1
            end


            if nun_cod2.present?
                barcode2 = Barby::Code128.new(nun_cod2)
                barcode_voorbeeld2 = Barby::HtmlOutputter.new(barcode2)
                barcode_htmlstring2 = barcode_voorbeeld2.to_html
                cod2 = '<div class="cod-2">
                            <div class="barcode-holder">
                                '+barcode_htmlstring2.html_safe+'
                                <p class="numero">'+product[1].to_s+'</p>
                                <p class="cod-num">'+product[5].to_s+'</p>
                            </div>
                        </div>'
                @codes2 << cod2
            end


            if nun_cod3.present?
                barcode3 = Barby::Code128.new(nun_cod3)
                barcode_voorbeeld3 = Barby::HtmlOutputter.new(barcode3)
                barcode_htmlstring3 = barcode_voorbeeld3.to_html
                
                unit_string = '';
                if product[8].to_i > 1
                    unit_string = 'UNIDADE'
                else
                    unit_string = 'UNIDADES'
                end

                cod3 = '<div class="cod-3">
                            <p class="titulo">CONTÉM: '+product[8].to_s+' UNIDADES</p>
                            <div class="barcode-holder">
                                '+barcode_htmlstring3.html_safe+'
                            </div>
                            <p class="numero-nome">'+product[1].to_s+' - '+product[2].to_s+'</p>
                            <p class="cod-num">'+product[6].to_s+'</p>
                        </div>'
                @codes3 << cod3
            end
            
        end
    end
end
