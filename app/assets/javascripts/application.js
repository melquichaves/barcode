// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require activestorage
//= require turbolinks
//= require jquery3
//= require jquery_ujs
//= require twitter/bootstrap
//= require_tree .

$(document).ready(function(){
    $('#load').click(function(e){
        e.preventDefault();
        e.stopPropagation();

        var data = $('#csv_data').val();
        var rows = data.split("\n");
        
        var tableView = $('<table class="table" />');

        tableView.append('<thead><tr><th scope="col">Material</th><th scope="col">Numero do produto</th><th scope="col">Nome do produto</th><th scope="col">Kit ou unidade</th><th scope="col">Código de barras 1</th><th scope="col">Código de barras 2</th><th scope="col">Código de barras 3</th><th scope="col">Quantidade de pedido</th><th scope="col">Unidades cod3</th></tr></thead>');
        tbody = $('<tbody />');
        tableView.append(tbody);

        for(var y in rows) {
            var cells = rows[y].split("\t");
            var row = $('<tr />');

            for(var x in cells) {
                row.append('<td>'+cells[x]+'</td>');
            }
            
            tbody.append(row);
        }
        
        // Insert into DOM
        $('#load-table-view').html(tableView);
    });


    $('#generate').click(function(){

        var data = $('#csv_data').val();
        var rows = data.split("\n");
        var table = [];


        for(var y in rows) {
            var cells = rows[y].split("\t");
            table.push(cells);
        }

        $.ajax({
            url : "/barcode",
            type : "post",
            data : { data_value: JSON.stringify(table) }
        });
    });
    
});

